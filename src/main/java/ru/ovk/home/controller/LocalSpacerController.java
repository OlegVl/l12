package ru.ovk.home.controller;

import ru.ovk.home.entity.LocalSpace;

import java.util.ArrayList;
import java.util.List;

public class LocalSpacerController {
    public List<LocalSpace> localSpaces = new ArrayList<>();

    public LocalSpace FindByUserId(Long userId)
    {
        if(userId==null) return null;
        for(final LocalSpace ls: localSpaces){
            if(ls.userId==userId) {return ls; }
        }

        return null;
    }

    public LocalSpace AddNewSpace(Long userId){
        LocalSpace localSpace=FindByUserId(userId);
        if(localSpace!=null) return localSpace;
        localSpace = new LocalSpace(userId);
        localSpaces.add(localSpace);
        return localSpace;
    }

    //соберём в одну функцию, для удобства




}
