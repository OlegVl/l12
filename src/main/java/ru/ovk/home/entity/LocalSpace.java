package ru.ovk.home.entity;

import ru.ovk.home.controller.ProjectController;
import ru.ovk.home.controller.TaskController;
import ru.ovk.home.repository.ProjectRepository;
import ru.ovk.home.repository.TaskRepository;
import ru.ovk.home.service.ProjectService;
import ru.ovk.home.service.ProjectTaskService;
import ru.ovk.home.service.TaskService;

public class LocalSpace {
    public Long userId=null;

    public ProjectRepository projectRepository;
    public ProjectService projectService;
    public ProjectController projectController;

    public TaskRepository taskRepository;
    public TaskService taskService;

    public ProjectTaskService projectTaskService;
    public TaskController taskController;

    public LocalSpace(Long userId) {
        this.userId = userId;
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService       = new ProjectService(projectRepository);
        ProjectController projectController = new ProjectController(projectService);
        TaskRepository taskRepository       = new TaskRepository();
        taskService                         = new TaskService(taskRepository);
        ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        TaskController taskController       = new TaskController(taskService, projectTaskService);
    }
}
