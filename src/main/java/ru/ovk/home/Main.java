package ru.ovk.home;

import ru.ovk.home.controller.*;
import ru.ovk.home.repository.*;
import ru.ovk.home.service.*;
import ru.ovk.home.enumerated.*;
import ru.ovk.home.entity.LocalSpace;

import java.util.*;
import static ru.ovk.home.constant.TerminalConst.*;

public class Main {
    private final SystemController systemController   = new SystemController();

    //private final ProjectRepository projectRepository = new ProjectRepository();
    //private final ProjectService projectService       = new ProjectService(projectRepository);
    //private final ProjectController projectController = new ProjectController(projectService);

    //private final TaskRepository taskRepository       = new TaskRepository();
    //private final TaskService    taskService          = new TaskService(taskRepository);

    private final UserRepository userRepository       = new UserRepository();
    private final UserService userService             = new UserService(userRepository);
    private final UserController userController       = new UserController(userService);

    private LocalSpacerController localSpacerController = new LocalSpacerController();
    private LocalSpace actualLocalSpace;

    //private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    //private final TaskController taskController       = new TaskController(taskService, projectTaskService);


    //для совместимости
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private ProjectController projectController;

    private TaskRepository taskRepository;
    private TaskService    taskService;

    private ProjectTaskService projectTaskService;
    private TaskController taskController;

    {
       // projectService.create("Demo Project 1");
       // projectService.create("Demo Project 2");
       // taskService.create("TEST Task 1");
       // taskService.create("TEST Task 2");

        userService.create("user1","pass1","FN1","SN1","F1");
        userService.create("user2","pass2","FN2","SN2","F2",Role.USER);

    }

    public static void main(String[] args) {
        final Scanner scaner  = new Scanner(System.in);
        final Main main = new Main();
        main.run(args);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scaner.nextLine();
            main.run(command);
        }
    }

    private void run(final String[] args) {
        String sCommand;
        if (args.length < 1) sCommand = "default";
        else sCommand = args[0];
        final int result = run(sCommand);
        //System.exit(result);
    }

    private int run(final String sCommand) {
        if (sCommand == null || sCommand.isEmpty()) {
            return 0;
        }

        if (actualLocalSpace != null) {
            switch (sCommand) {
                case CMD_ABOUT:
                    return systemController.displayAbout();
                case CMD_VERSION:
                    return systemController.displayVersion();
                case CMD_HELP:
                    return systemController.displayHelp();
                case CMD_DEFAULT:
                    return systemController.displayDefault();
                case CMD_EXIT:
                    return systemController.displayExit();

                case PROJECT_CLEAR:
                    return projectController.clearProject();
                case PROJECT_CREATE:
                    return projectController.createProject();
                case PROJECT_LIST:
                    return projectController.listProject();
                case PROJECT_VIEW:
                    return projectController.viewProjectByIndex();
                case PROJECT_VIEW_BY_INDEX:
                    return projectController.viewProjectByIndex();
                case PROJECT_VIEW_BY_ID:
                    return projectController.viewProjectById();
                case PROJECT_VIEW_BY_NAME:
                    return projectController.viewProjectByName();
                case PROJECT_REMOVE_BY_INDEX:
                    return projectController.removeProjectByIndex();
                case PROJECT_REMOVE_BY_ID:
                    return projectController.removeProjectById();
                case PROJECT_REMOVE_BY_NAME:
                    return projectController.removeProjectByName();
                case PROJECT_UPDATE_BY_INDEX:
                    return projectController.updateProjectByIndex();
                case PROJECT_UPDATE_BY_ID:
                    return projectController.updateProjectById();

                case TASK_CLEAR:
                    return taskController.clearTask();
                case TASK_CREATE:
                    return taskController.createTask();
                case TASK_LIST:
                    return taskController.listTask();
                case TASK_VIEW_BY_INDEX:
                    return taskController.viewTaskByIndex();
                case TASK_VIEW_BY_ID:
                    return taskController.viewTaskById();
                case TASK_VIEW_BY_NAME:
                    return taskController.viewTaskByName();
                case TASK_REMOVE_BY_INDEX:
                    return taskController.removeTaskByIndex();
                case TASK_REMOVE_BY_ID:
                    return taskController.removeTaskById();
                case TASK_REMOVE_BY_NAME:
                    return taskController.removeTaskByName();
                case TASK_UPDATE_BY_INDEX:
                    return taskController.updateTaskByIndex();
                case TASK_UPDATE_BY_ID:
                    return taskController.updateTaskById();

                case TASK_ADD_PROJECT_BY_IDS:
                    return taskController.addTaskToProjectByIds();
                case TASK_REMOVE_PROJECT_BY_IDS:
                    return taskController.removeTaskProjectByIds();
                case TASK_LIST_BY_PROJECT_ID:
                    return taskController.listTaskByProjectId();

                case USER_CREATE:
                    return userController.createUser();
                case ADMIN_CREATE:
                    return userController.createUser(Role.ADMIN);
                case USERS_CLEAR:
                    return userController.clearUsers();
                case USERS_LIST:
                    return userController.listUsers();
                case USER_VIEW_BY_LOGIN:
                    return userController.viewUserByLogin();
                case USER_REMOVE_BY_LOGIN:
                    return userController.removeUserByLogin();
                case USER_UPDATE_BY_LOGIN:
                    return userController.updateUserByLogin();
                case USER_VIEW_BY_ID:
                    return userController.viewUserById();
                case USER_REMOVE_BY_ID:
                    return userController.removeUserById();
                case USER_UPDATE_BY_ID:
                    return userController.updateUserById();

                case USER_LOGIN:
                    return uLogin();
                case USER_CHANGE_PASS:
                    return userController.userChangePass(userController.actualUserId);
                case USER_VIEW_PROFILE:
                    return userController.viewProfile(userController.actualUserId);
                case USER_UPDATE_PROFILE:
                    return userController.updateProfile(userController.actualUserId);
                case USER_LOGOUT:
                    return userController.userLogout(userController.actualUserId);

                default:
                    return systemController.displayErr();
            }
        }
        else {
            switch (sCommand) {
                case CMD_ABOUT:
                    return systemController.displayAbout();
                case CMD_VERSION:
                    return systemController.displayVersion();
                case CMD_HELP:
                    return systemController.displayHelp();
                case CMD_DEFAULT:
                    return systemController.displayDefault();
                case CMD_EXIT:
                    return systemController.displayExit();

                case USER_LOGIN:
                    return uLogin();
                case USER_CHANGE_PASS:
                    return userController.userChangePass(userController.actualUserId);
                case USER_VIEW_PROFILE:
                    return userController.viewProfile(userController.actualUserId);
                case USER_UPDATE_PROFILE:
                    return userController.updateProfile(userController.actualUserId);
                case USER_LOGOUT:
                    return userController.userLogout(userController.actualUserId);

                default:
                    return systemController.displayErr();
            }
       }


    }


    private int uLogin() {
        if(userController.login()>0) {
            actualLocalSpace=localSpacerController.AddNewSpace(userController.actualUserId);
            projectRepository = actualLocalSpace.projectRepository;
            projectService = actualLocalSpace.projectService;
            projectController = actualLocalSpace.projectController;
            taskRepository = actualLocalSpace.taskRepository;
            taskService = actualLocalSpace.taskService;
            projectTaskService = actualLocalSpace.projectTaskService;
            taskController = actualLocalSpace.taskController;
        }
      return 0;
  }




}